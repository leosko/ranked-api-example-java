package com.seapy;

import com.google.gson.*;
import com.seapy.replay.ProtoReplayItems.*;

import java.io.*;
import java.util.*;
import java.util.Map.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

import static com.seapy.Mapping.*;
import static com.seapy.Utils.*;
import static java.lang.Integer.parseInt;

public class Example {

    public static final ExecutorService executorService = Executors.newFixedThreadPool(24);

    public static void main(String[] args) throws InterruptedException, IOException{
        JsonElement serverContent = getServerContent();
        Map<Integer, Ratio> unitsRatioMap = new ConcurrentHashMap<>();
        Map<Integer, Ratio> blocksRatioMap = new ConcurrentHashMap<>();
        Map<Integer, AtomicInteger> unitsParticipation = new ConcurrentHashMap<>();
        Map<Integer, AtomicInteger> blocksParticipation = new ConcurrentHashMap<>();
        Map<Integer, AtomicInteger> unitsWinningParticipation = new ConcurrentHashMap<>();
        Map<Integer, AtomicInteger> blocksWinningParticipation = new ConcurrentHashMap<>();
        Map<Integer, Integer> unitPoints = new ConcurrentHashMap<>();
        Map<Integer, Integer> blockPoints = new ConcurrentHashMap<>();
        Map<Integer, AtomicInteger> blockWinningParticipation = new ConcurrentHashMap<>();
        AtomicInteger nonAIMatch = new AtomicInteger(0);

        // Season 8
        int minMatch = 107612;
        int maxMatch = 113888;

        for (int currentMatchId = minMatch; currentMatchId < maxMatch; currentMatchId++) {
            int finalCurrentMatchId = currentMatchId;
            executorService.submit(() -> {
                try {
                    JsonObject matchInfo = getMatchInfo(finalCurrentMatchId).getAsJsonObject();
                    if (!matchInfo.get("type").getAsString().equals("1v1")) {
                        return;
                    }
                    JsonObject players = matchInfo.getAsJsonObject("players");
                    Iterator<String> iterator = players.keySet().iterator();
                    String team1PlayerIdStr = iterator.next();
                    String team2PlayerIdStr = iterator.next();
                    if (team1PlayerIdStr.startsWith("HID") || team2PlayerIdStr.startsWith("HID")){
                        return;
                    }
                    int team1PlayerId = parseInt(team1PlayerIdStr);
                    int team2PlayerId = parseInt(team2PlayerIdStr);
                    JsonObject player1 = players.getAsJsonObject(team1PlayerIdStr);
                    JsonObject player2 = players.getAsJsonObject(team2PlayerIdStr);

                    int winnerTeamId = player1.getAsJsonPrimitive("place").getAsInt() == 1 ?
                        player1.getAsJsonPrimitive("team").getAsInt() :
                        player2.getAsJsonPrimitive("team").getAsInt();
                    int loserTeamId = player1.getAsJsonPrimitive("place").getAsInt() == 1 ?
                        player2.getAsJsonPrimitive("team").getAsInt() :
                        player1.getAsJsonPrimitive("team").getAsInt();
                    long startTimestamp = matchInfo.getAsJsonPrimitive("started_at").getAsLong();
                    long finishTimestamp = matchInfo.getAsJsonPrimitive("finished_at").getAsLong();
                    if (finishTimestamp - startTimestamp > 10 * 60 * 1000
                        && (team1PlayerId > 10 || team1PlayerId == 0)
                        && (team2PlayerId > 10 || team2PlayerId == 0)
                        && player1.has("rating_before") && player1.getAsJsonPrimitive("rating_before").getAsInt() > 2000
                        && player2.has("rating_before") && player2.getAsJsonPrimitive("rating_before").getAsInt() > 2000) {
                        nonAIMatch.incrementAndGet();
                        try {
                            List<ProtoReplay> events = getParsedProtoEvents(finalCurrentMatchId);
                            Map<Integer, Set<Integer>> teamToUnits = new HashMap<>();
                            Map<Integer, Set<Integer>> teamToBlocks = new HashMap<>();
                            for (ProtoReplay event : events) {
                                if (event.getType().equals(ProtoReplayItemType.UNIT_UNLOAD)) {
                                    ProtoUnit protoUnit = ProtoUnit.parseFrom(event.getItem());
                                    if (teamToUnits.containsKey(event.getTeamId())) {
                                        teamToUnits.get(event.getTeamId()).add(protoUnit.getUnitType());
                                    } else {
                                        HashSet<Integer> value = new HashSet<>();
                                        value.add(protoUnit.getUnitType());
                                        teamToUnits.put(event.getTeamId(), value);
                                    }
                                    if (unitsRatioMap.containsKey(protoUnit.getUnitType())) {
                                        unitsRatioMap.get(protoUnit.getUnitType()).created++;
                                    } else {
                                        unitsRatioMap.put(protoUnit.getUnitType(), new Ratio(1, 0));
                                    }
                                    if (event.getTeamId() == winnerTeamId) {
                                        if (unitsWinningParticipation.containsKey(protoUnit.getUnitType())) {
                                            unitsWinningParticipation.get(protoUnit.getUnitType()).incrementAndGet();
                                        } else {
                                            unitsWinningParticipation.put(protoUnit.getUnitType(), new AtomicInteger(1));
                                        }
                                    } else if (event.getTeamId() == loserTeamId) {
                                        if (unitsWinningParticipation.containsKey(protoUnit.getUnitType())) {
                                            unitsWinningParticipation.get(protoUnit.getUnitType()).decrementAndGet();
                                        } else {
                                            unitsWinningParticipation.put(protoUnit.getUnitType(), new AtomicInteger(-1));
                                        }
                                    }
                                    // if (event.getTeamId() == matchInfo.getAsJsonObject().getAsJsonPrimitive("result").getAsInt()) {
                                    //     unitPoints.merge(protoUnit.getUnitType(), 1, (old, newValue) -> old - newValue);
                                    // }
                                } else if (event.getType().equals(ProtoReplayItemType.UNIT_DESTROY)) {
                                    ProtoUnit protoUnit = ProtoUnit.parseFrom(event.getItem());
                                    if (unitsRatioMap.containsKey(protoUnit.getUnitType())) {
                                        unitsRatioMap.get(protoUnit.getUnitType()).destroyed++;
                                    } else {
                                        unitsRatioMap.put(protoUnit.getUnitType(), new Ratio(0, 1));
                                    }
                                    // if (event.getTeamId() == matchInfo.getAsJsonObject().getAsJsonPrimitive("result").getAsInt()) {
                                    //     unitPoints.merge(protoUnit.getUnitType(), 1, (old, newValue) -> old - newValue);
                                    // }
                                } else if (event.getType().equals(ProtoReplayItemType.BLOCK_BUILD_END)) {
                                    ProtoBlock protoBlock = ProtoBlock.parseFrom(event.getItem());
                                    if (!protoBlock.getBreaking()) {
                                        if (teamToBlocks.containsKey(event.getTeamId())) {
                                            teamToBlocks.get(event.getTeamId()).add(protoBlock.getBlockId());
                                        } else {
                                            HashSet<Integer> value = new HashSet<>();
                                            value.add(protoBlock.getBlockId());
                                            teamToBlocks.put(event.getTeamId(), value);
                                        }
                                        if (event.getTeamId() == winnerTeamId) {
                                            if (blocksWinningParticipation.containsKey(protoBlock.getBlockId())) {
                                                blocksWinningParticipation.get(protoBlock.getBlockId()).incrementAndGet();
                                            } else {
                                                blocksWinningParticipation.put(protoBlock.getBlockId(), new AtomicInteger(1));
                                            }
                                        } else if (event.getTeamId() == loserTeamId) {
                                            if (blocksWinningParticipation.containsKey(protoBlock.getBlockId())) {
                                                blocksWinningParticipation.get(protoBlock.getBlockId()).decrementAndGet();
                                            } else {
                                                blocksWinningParticipation.put(protoBlock.getBlockId(), new AtomicInteger(-1));
                                            }
                                        }

                                        if (blocksRatioMap.containsKey(protoBlock.getBlockId())) {
                                            blocksRatioMap.get(protoBlock.getBlockId()).created++;
                                        } else {
                                            blocksRatioMap.put(protoBlock.getBlockId(), new Ratio(1, 0));
                                        }
                                        if (event.getTeamId() == winnerTeamId) {
                                            blockPoints.merge(protoBlock.getBlockId(), 1, Integer::sum);
                                        }
                                        if (event.getTeamId() == winnerTeamId) {
                                            if (blockWinningParticipation.containsKey(protoBlock.getBlockId())) {
                                                blockWinningParticipation.get(protoBlock.getBlockId()).incrementAndGet();
                                            } else {
                                                blockWinningParticipation.put(protoBlock.getBlockId(), new AtomicInteger(1));
                                            }
                                        } else if (event.getTeamId() == loserTeamId) {
                                            if (blockWinningParticipation.containsKey(protoBlock.getBlockId())) {
                                                blockWinningParticipation.get(protoBlock.getBlockId()).decrementAndGet();
                                            } else {
                                                blockWinningParticipation.put(protoBlock.getBlockId(), new AtomicInteger(-1));
                                            }
                                        }
                                    }
                                } else if (event.getType().equals(ProtoReplayItemType.BLOCK_DESTROY)) {
                                    ProtoBlock protoBlock = ProtoBlock.parseFrom(event.getItem());
                                    if (blocksRatioMap.containsKey(protoBlock.getBlockId())) {
                                        blocksRatioMap.get(protoBlock.getBlockId()).destroyed++;
                                    } else {
                                        blocksRatioMap.put(protoBlock.getBlockId(), new Ratio(0, 1));
                                    }
                                    if (event.getTeamId() == winnerTeamId) {
                                        blockPoints.merge(protoBlock.getBlockId(), 1, (old, newValue) -> old - newValue);
                                    }
                                }
                            }

                             for (Entry<Integer, Set<Integer>> entry : teamToBlocks.entrySet()) {
                                 for (Integer unitType : entry.getValue()) {
                                     if (blocksParticipation.containsKey(unitType)) {
                                         blocksParticipation.get(unitType).incrementAndGet();
                                     } else {
                                         blocksParticipation.put(unitType, new AtomicInteger(1));
                                     }
                                 }
                             }

                             for (Entry<Integer, Set<Integer>> entry : teamToUnits.entrySet()) {
                                 for (Integer unitType : entry.getValue()) {
                                     if (unitsParticipation.containsKey(unitType)) {
                                         unitsParticipation.get(unitType).incrementAndGet();
                                     } else {
                                         unitsParticipation.put(unitType, new AtomicInteger(1));
                                     }
                                 }
                             }

                            Set<Integer> winnerUnits = teamToUnits.get(winnerTeamId);
                            Set<Integer> loserUnits = teamToUnits.get(loserTeamId);
                            if (winnerUnits == null || loserUnits == null) {
                                return;
                            }
                            //flare
                            for (int i = 15; i <= 18; i++) {
                                if (winnerUnits.contains(i) && !winnerUnits.contains(i + 1)) {
                                    unitPoints.merge(i, 1, Integer::sum);
                                } else if (i == 18 && winnerUnits.contains(i + 1)) {
                                    unitPoints.merge(i + 1, 1, Integer::sum);
                                }
                                if (loserUnits.contains(i) && !loserUnits.contains(i + 1)) {
                                    unitPoints.merge(i, 1, (integer, integer2) -> integer - integer2);
                                } else if (i == 18 && loserUnits.contains(i + 1)) {
                                    unitPoints.merge(i + 1, 1, (integer, integer2) -> integer - integer2);
                                }
                            }
                            //dagger
                            for (int i = 0; i <= 3; i++) {
                                if (winnerUnits.contains(i) && !winnerUnits.contains(i + 1)) {
                                    unitPoints.merge(i, 1, Integer::sum);
                                } else if (i == 3 && winnerUnits.contains(i + 1)) {
                                    unitPoints.merge(i + 1, 1, Integer::sum);
                                }
                                if (loserUnits.contains(i) && !loserUnits.contains(i + 1)) {
                                    unitPoints.merge(i, 1, (integer, integer2) -> integer - integer2);
                                } else if (i == 3 && loserUnits.contains(i + 1)) {
                                    unitPoints.merge(i + 1, 1, (integer, integer2) -> integer - integer2);
                                }
                            }
                            //nova
                            for (int i = 5; i <= 8; i++) {
                                if (winnerUnits.contains(i) && !winnerUnits.contains(i + 1)) {
                                    unitPoints.merge(i, 1, Integer::sum);
                                } else if (i == 8 && winnerUnits.contains(i + 1)) {
                                    unitPoints.merge(i + 1, 1, Integer::sum);
                                }
                                if (loserUnits.contains(i) && !loserUnits.contains(i + 1)) {
                                    unitPoints.merge(i, 1, (integer, integer2) -> integer - integer2);
                                } else if (i == 8 && loserUnits.contains(i + 1)) {
                                    unitPoints.merge(i + 1, 1, (integer, integer2) -> integer - integer2);
                                }
                            }
                            //crawler
                            for (int i = 10; i <= 13; i++) {
                                if (winnerUnits.contains(i) && !winnerUnits.contains(i + 1)) {
                                    unitPoints.merge(i, 1, Integer::sum);
                                } else if (i == 13 && winnerUnits.contains(i + 1)) {
                                    unitPoints.merge(i + 1, 1, Integer::sum);
                                }
                                if (loserUnits.contains(i) && !loserUnits.contains(i + 1)) {
                                    unitPoints.merge(i, 1, (integer, integer2) -> integer - integer2);
                                } else if (i == 13 && loserUnits.contains(i + 1)) {
                                    unitPoints.merge(i + 1, 1, (integer, integer2) -> integer - integer2);
                                }
                            }
                            //risso
                            for (int i = 25; i <= 28; i++) {
                                if (winnerUnits.contains(i) && !winnerUnits.contains(i + 1)) {
                                    unitPoints.merge(i, 1, Integer::sum);
                                } else if (i == 28 && winnerUnits.contains(i + 1)) {
                                    unitPoints.merge(i + 1, 1, Integer::sum);
                                }
                                if (loserUnits.contains(i) && !loserUnits.contains(i + 1)) {
                                    unitPoints.merge(i, 1, (integer, integer2) -> integer - integer2);
                                } else if (i == 28 && loserUnits.contains(i + 1)) {
                                    unitPoints.merge(i + 1, 1, (integer, integer2) -> integer - integer2);
                                }
                            }
                            //mono
                            for (int i = 20; i <= 23; i++) {
                                if (winnerUnits.contains(i) && !winnerUnits.contains(i + 1)) {
                                    unitPoints.merge(i, 1, Integer::sum);
                                } else if (i == 23 && winnerUnits.contains(i + 1)) {
                                    unitPoints.merge(i + 1, 1, Integer::sum);
                                }
                                if (loserUnits.contains(i) && !loserUnits.contains(i + 1)) {
                                    unitPoints.merge(i, 1, (integer, integer2) -> integer - integer2);
                                } else if (i == 23 && loserUnits.contains(i + 1)) {
                                    unitPoints.merge(i + 1, 1, (integer, integer2) -> integer - integer2);
                                }
                            }
                        } catch (Exception exception) {
                            System.out.println("Error processing " + finalCurrentMatchId);
                            exception.printStackTrace();
                        }
                        System.out.println(finalCurrentMatchId);
                    }
                } catch (Exception exception) {
                    System.out.println("Error processing " + finalCurrentMatchId);
                    exception.printStackTrace();
                }
            });
        }

        executorService.shutdown();
        executorService.awaitTermination(15, TimeUnit.MINUTES);

        System.out.println("Non-AI matches: " + nonAIMatch.get());
        System.out.println("unit_type,created,destroyed,death_ratio,unitPoints,points_ratio,parts,winning_parts,relative_cost");
        for (Integer unitType : unitTypeToName.keySet()) {
        // for (Entry<Integer, Ratio> entry : unitsRatioMap.entrySet()) {
            if (unitsRatioMap.containsKey(unitType)) {
                Ratio ratio = unitsRatioMap.get(unitType);
                System.out.printf("%s,%d,%d,%f,%d,%f,%d,%d,%f%n",
                    unitTypeToName.get(unitType),
                    ratio.created,
                    ratio.destroyed,
                    (float) ratio.destroyed / ratio.created,
                    unitPoints.getOrDefault(unitType, 0),
                    (float) unitPoints.getOrDefault(unitType, 0) / unitsParticipation.getOrDefault(unitType, new AtomicInteger(0)).get(),
                    unitsParticipation.getOrDefault(unitType, new AtomicInteger(0)).get(),
                    unitsWinningParticipation.getOrDefault(unitType, new AtomicInteger(0)).get(),
                    serverContent.getAsJsonObject().getAsJsonObject("units").getAsJsonObject(unitType.toString()).getAsJsonPrimitive("relativeCost").getAsDouble());
            } else {
                System.out.printf("%s,0,0,0,0,0,0,0,%f%n",
                unitTypeToName.get(unitType),
                serverContent.getAsJsonObject().getAsJsonObject("units").getAsJsonObject(unitType.toString()).getAsJsonPrimitive("relativeCost").getAsDouble());
            }
        }
        System.out.println("block_type,created,destroyed,death_ratio,blockPoints,points_ratio,parts,winning_parts,relative_cost");
        for (Integer blockType : blockTypeToName.keySet()) {
            if (blocksRatioMap.containsKey(blockType)){
                Ratio ratio = blocksRatioMap.get(blockType);
                System.out.printf("%s,%d,%d,%f,%d,%f,%d,%d,%f%n",
                    blockTypeToName.get(blockType),
                    ratio.created,
                    ratio.destroyed,
                    (float) ratio.destroyed / ratio.created,
                    blockPoints.getOrDefault(blockType, 0),
                    (float) blockPoints.getOrDefault(blockType, 0) / blocksParticipation.getOrDefault(blockType, new AtomicInteger(0)).get(),
                    blocksParticipation.getOrDefault(blockType, new AtomicInteger(0)).get(),
                    blocksWinningParticipation.getOrDefault(blockType, new AtomicInteger(0)).get(),
                    serverContent.getAsJsonObject().getAsJsonObject("blocks").getAsJsonObject(blockType.toString()).getAsJsonPrimitive("relativeCost").getAsDouble());
            } else {
                System.out.printf("%s,0,0,0,0,0,0,0,%f%n",
                blockTypeToName.get(blockType),
                serverContent.getAsJsonObject().getAsJsonObject("blocks").getAsJsonObject(blockType.toString()).getAsJsonPrimitive("relativeCost").getAsDouble());
            }
        }
    }
}
