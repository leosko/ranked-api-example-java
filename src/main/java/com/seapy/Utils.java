package com.seapy;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.seapy.replay.ProtoReplayItems.ProtoReplay;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipInputStream;

public class Utils {
    static Path tempDir = Path.of(System.getProperty("java.io.tmpdir"), "ranked_cache");

    // Produces URL for getting unit events from match
    public static String getURLForReplay(int matchId) {
        return "https://ranked.ddns.net/replay/" + matchId + ".zip";
    }

    // https://stackoverflow.com/a/1359702/7064164
    public static InputStreamReader getURLRaw(String url) throws IOException {
        URL url1 = new URL(url);
        return new InputStreamReader(url1.openStream());
    }

    // https://stackoverflow.com/a/1359702/7064164
    public static InputStream getZIPURLRaw(String url) throws IOException {
        URL url1 = new URL(url);
        URLConnection yc = url1.openConnection();
        ZipInputStream zipInputStream = new ZipInputStream(yc.getInputStream());
        zipInputStream.getNextEntry();
        return zipInputStream;
    }

    public static InputStream getZIPOrCache(int matchId) throws IOException {
        String urlForReplay = getURLForReplay(matchId);
        Path cachePath = Path.of(tempDir.toAbsolutePath().toString(), matchId + ".zip");
        Files.createDirectories(tempDir);
        if (!Files.exists(cachePath)) {
            URL url1 = new URL(urlForReplay);
            URLConnection yc = url1.openConnection();
            System.out.println("Saving new file into " + cachePath.toAbsolutePath());
            File cacheFile = cachePath.toFile();
            if (!cacheFile.createNewFile()) {
                throw new IOException("Can't create file " + cacheFile.getAbsolutePath());
            }
            FileOutputStream fileOutputStream = new FileOutputStream(cacheFile);
            fileOutputStream.write(yc.getInputStream().readAllBytes());
            fileOutputStream.close();
        }

        System.out.println("Reading file " + cachePath.toAbsolutePath());
        ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(cachePath.toFile()));
        zipInputStream.getNextEntry();
        return zipInputStream;
    }

    public static List<ProtoReplay> getParsedProtoEvents(int matchId) throws IOException {
        List<ProtoReplay> res = new LinkedList<>();
        InputStream input = getZIPOrCache(matchId);
        try {
            ProtoReplay value = ProtoReplay.parseDelimitedFrom(input);
            while (value != null) {
                res.add(value);
                value = ProtoReplay.parseDelimitedFrom(input);
            }
        } catch (IOException e) {
            // finished
        }
        return res;
    }

    public static JsonElement getMatchInfo(int matchId) throws IOException {
        return JsonParser.parseReader(
            new JsonReader(
                getURLRaw("https://ranked.ddns.net/api/match?matchId=" + matchId)));
    }

    public static JsonElement getServerContent() throws IOException {
        return JsonParser.parseReader(
            new JsonReader(
                getURLRaw("https://ranked.ddns.net/api/serverContent")));
    }
}
